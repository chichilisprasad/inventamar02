﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
namespace EntityModel
{
    [DataContract]
    public class FamilyMember
    {
        [DataMember]
        [Required]
        public string CLIENTID { get; set; }
        [DataMember]
        [Required]
        public string CSCID { get; set; }
        [DataMember]
        [Required]
        public string ORWID { get; set; }
        [DataMember]
        public Nullable<int> RELATIONSHIP { get; set; }
        [DataMember]
        public string NAME { get; set; }
        [DataMember]
        public Nullable<int> AGE { get; set; }
        [DataMember]
        public Nullable<int> MEMBERCLIENTID { get; set; }
        [DataMember]
        public Nullable<int> HIVSTATUS { get; set; }
        [DataMember]
        public Nullable<int> TBSTATUS { get; set; }
        [DataMember]
        public string TBREMARKS { get; set; }
        [DataMember]
        public Nullable<int> GENDER { get; set; }
        [DataMember]
        public Nullable<int> LIVINGSTATUS { get; set; }
        [DataMember]
        public Nullable<int> CD4COUNTIFPOSITIVE { get; set; }
        [DataMember]
        public Nullable<int> WILLINGFORSOCIALSCHEMES { get; set; }
        [DataMember]
        public Nullable<int> ARTSTATUS { get; set; }
        [DataMember]
        public string REGNUMBER { get; set; }
        [DataMember]
        public Nullable<int> NUMELIGIBLEMEMBERS { get; set; }
        [DataMember]
        public Nullable<System.DateTime> CREATEDDATE { get; set; }
        [DataMember]
        public Nullable<System.DateTime> UPDATEDDATE { get; set; }
        [DataMember]
        public string FAMILYID { get; set; }
        [DataMember]
        public long fldRowID { get; set; }
        [DataMember]
        public string fldCreatedBy { get; set; }
        [DataMember]
        public Nullable<DateTime> DateHIVdetection { get; set; }

    }
}
